from fitconnect import FITConnectClient, Environment
from flask import Flask, render_template
from jwcrypto.jwe import InvalidJWEData
from os import path
from strictyaml import load, Map, Str, Int, Seq, YAMLError
from strictyaml import Enum as YAMLEnum
from jwcrypto import jwk
import json
import jsonschema
import logging

# configure logging
logging.basicConfig()
logging.getLogger('fitconnect').level = logging.INFO

# parse yaml config
with open('config.yaml') as file:
    config_schema = Map({
        "private_key_decryption_file": Str(),
        "environment": YAMLEnum([e.name for e in Environment]), # change to native Enum when strictyaml supports it: https://github.com/crdoconnor/strictyaml/issues/73
        "client_id": Str(),
        "client_secret": Str(),
    })

    config = load(file.read(), config_schema, label='config.yaml').data

    # load private key for decryption
    with open(config['private_key_decryption_file']) as private_key_file:
        config['private_key_decryption'] = json.load(private_key_file)

# initialize SDK
fitc = FITConnectClient(Environment[config['environment']], config['client_id'], config['client_secret'])

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/submissions/<uuid:submission_id>')
def check_submission_json(submission_id):
    # check conformance of given submission by submission id
    conformance = {
        'submission_exists': {
            'description': 'The submission has been submitted successfully and could be retrieved.',
            'status': 'pending'
        },
        'metadata_decryption': {
            'description': 'Metadata decrypted successfully.',
            'status': 'pending'
        },
        'metadata_schema': {
            'description': 'Metadata is schema compliant.',
            'status': 'pending'
        },
        'data_decrypt': {
            'description': 'Data (Fachdaten) decrypted successfully.',
            'status': 'pending'
        },
        'metadata_data_hash': {
            'description': 'Data (Fachdaten) hash value from metadata matches the actual data hash.',
            'status': 'pending'
        },
        'data_json': {
            'description': 'Data (Fachdaten) is valid JSON.',
            'status': 'pending'
        },
    }

    def conformance_attachment_exists(attachment_id, status = 'pending'):
        return {
            'description': f'Attachment with attachment_id {attachment_id} exists.',
            'status': status,
        }

    def conformance_attachment_decryption(attachment_id, status = 'pending'):
        return {
            'description': f'Attachment with attachment_id {attachment_id} decrypted successfully.',
            'status': status,
        }

    def conformance_attachment_hash(attachment_id, status = 'pending'):
        return {
            'description': f'Hash value of attachment with attachment_id {attachment_id} from metadata matches the actual attachment hash.',
            'status': status,
        }

    try:
        print(f"\n== Retrieving submission {submission_id} ==")
        private_key = jwk.JWK.from_json(json.dumps(config['private_key_decryption']))

        # download submission
        submission = fitc._get_submission(submission_id)
        conformance['submission_exists']['status'] = 'success'
        print('a', conformance)

        # decrypt and validate metadata
        submission['metadata'] = fitc.decrypt_json(private_key, submission['encryptedMetadata']) # TODO: error handling
        conformance['metadata_decryption']['status'] = 'success'

        try:
            fitc._validate_metadata_schema(submission['metadata'])
            #raise ValueError('simulated metadata schema validation error')
            conformance['metadata_schema']['status'] = 'success'
        except ValueError as e:
            conformance['metadata_schema']['error'] = e
            conformance['metadata_schema']['status'] = 'error'
            raise e

        # decrypt and validata data
        data_decrypted = fitc.decrypt(private_key, submission['encryptedData']) # TODO: error handling
        conformance['data_decrypt']['status'] = 'success'
        fitc.verify_metadata_data_hash(submission['metadata'], data_decrypted)
        conformance['metadata_data_hash']['status'] = 'success'

        try:
            submission['data_json'] = json.loads(data_decrypted)
        except json.decoder.JSONDecodeError as e:
            raise e # TODO: decode xml
        conformance['data_json']['status'] = 'success'

        # handle attachments
        attachment_ids = submission['attachments']
        attachments = {}
        for attachment_id in attachment_ids:
            conformance[f'attachment-{attachment_id}-exists'] = conformance_attachment_exists(attachment_id, 'pending')
            r_get_attachment = fitc._authorized_get(f'/submissions/{submission_id}/attachments/{attachment_id}')
            conformance[f'attachment-{attachment_id}-exists'] = conformance_attachment_exists(attachment_id, 'success')

            conformance[f'attachment-{attachment_id}-decryption'] = conformance_attachment_decryption(attachment_id, 'pending')
            attachments[attachment_id] = fitc.decrypt(private_key, r_get_attachment.text) # TODO: error handling
            conformance[f'attachment-{attachment_id}-decryption'] = conformance_attachment_decryption(attachment_id, 'success')

            # verify hash values from metadata for attachment
            conformance[f'attachment-{attachment_id}-metadata_hash'] = conformance_attachment_hash(attachment_id, 'pending')
            fitc.verify_metadata_attachment_hash(submission['metadata'], attachment_id, attachments[attachment_id])
            conformance[f'attachment-{attachment_id}-metadata_hash'] = conformance_attachment_hash(attachment_id, 'success')

        submission['attachments'] = attachments

    except InvalidJWEData as e:
        print(f"Could not decrypt submission {submission_id}")
    except jsonschema.exceptions.ValidationError as e:
        print(f"Invalid schema in submission {submission_id}:", e)
    except json.decoder.JSONDecodeError as e:
        print(f"Unparsable json in submission {submission_id}")
    except ValueError as e:
        print("ValueError", e)
        return "ValueError: " + str(e)
    except Exception as e:
        print("Exception", e)
    finally:
        print(conformance)
        return render_template('submission.html', submission_id=submission_id, conformance_results=conformance)
        #return 'Submission %s has the following conformance results: <pre>%s</pre>' % (submission_id, json.dumps(conformance, indent=2, ensure_ascii=False).encode('utf-8').decode())
